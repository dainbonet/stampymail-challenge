
function action(actionType="POST", URL, callback){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            callback();
        }
    };
    xhttp.open(actionType, URL, true);
    xhttp.send();
}

function delete_user(id){
    let url = `delete.php?id=${id}`;
    let confirm = window.confirm("Are you sure?")
    if(confirm){
        action("POST", url, post_delete);
    }
    
function post_delete(){
    location.reload();
    console.log('delete');
}
}




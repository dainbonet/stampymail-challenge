<?php

// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}
require_once '../functions.php';

$result = false;

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty(trim($_REQUEST["id"]))){
        $result = false;
    }else {
        $param_iduser = trim($_REQUEST["id"]);
        $result = UserController::delete($param_iduser);
    }
    echo $result;
}
?>
<?php
// Initialize the session
session_start();
$table = '';

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}


require_once "../functions.php";


$result = UserController::getAll();


if ($result["error"] == '') {

    $table .= "<table class='table table-hover'>
            <thead class='thead-dark'>
            <tr>
            <th scope='col'>First Name</th>
            <th scope='col'>Last Name</th>
            <th scope='col'>Username</th>
            <th scope='col'>Email </th>
            <th scope='col' colspan='2'>Actions </th>
            </tr>
            </thead>
            <tbody>";
    if (isset($result["col"])) {
        foreach ($result["col"] as $key) {
            $user = $key;
            $table .= ("<tr><td>");
            $table .= $user->get_name();
            $table .= ("</td><td>");
            $table .= $user->get_lastName();
            $table .= ("</td><td>");
            $table .= $user->get_username();
            $table .= ("</td><td>");
            $table .= $user->get_email();
            $table .= "</td><td>
                <a href='register.php?id=" . strval($user->get_idUser()) . "' class='btn btn-primary' /> Edit </a>
                
                <a href='#' onclick='delete_user(\"" . strval($user->get_idUser()) . "\");' class='btn btn-danger'/> Delete </a>
                </td><td>
                </td></tr>";
        }
    }

    $table .= "</tbody></table>";
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/main.js"></script>
    <style>
        body {
            font: 14px sans-serif;
            text-align: center;
        }
    </style>
</head>

<body>
    <h1 class="my-5">Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>.<br> Welcome to Stampymail challenge.</h1>
    <p>
        <a href="register.php" class="btn btn-success ml-3">Add User</a>
        <a href="logout.php" class="btn btn-danger ml-3">Sign Out of Your Account</a>

    </p>
    <?php echo $table; ?>
</body>

</html>

</html>
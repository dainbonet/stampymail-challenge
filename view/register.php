<?php

require_once "../functions.php";

$name = $lastname = $username = $password = $confirm_password = $email = $result = "";
$username_err = $password_err = $confirm_password_err = $email_err = "";
$param = array();

$title = 'Sign up';

session_start();

if ($_SERVER["REQUEST_METHOD"] == "GET") {

    if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] == true) {

        if (isset($_GET["id"])) {
            $_SESSION["ADD"] = false;
            $_SESSION["EDIT"] = true;
            $title = 'Edit user';
            $param_idUser = trim($_GET["id"]);
            $result = UserController::get_by_id($param_idUser);
            if ($result["error"] == '') {
                if (isset($result["user"])) {
                    $name = $result["user"]->get_name();
                    $lastname = $result["user"]->get_lastname();
                    $username = $result["user"]->get_username();
                    $email = $result["user"]->get_email();
                }
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }
        } else {
            $_SESSION["EDIT"] = false;
            $_SESSION["ADD"] = true;
            $title = "New user";
        }
    }
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Validate username
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter a username.";
    } elseif (!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["username"]))) {
        $username_err = "Username can only contain letters, numbers, and underscores.";
    } else {
        if (!(isset($_SESSION["EDIT"]))) {
            // Set parameters
            $param_username = trim($_POST["username"]);

            $result = UserController::get_by_username($param_username);
            if ($result["error"] == '') {
                if (isset($result["user"])) {
                    $username_err = "This username is already taken.";
                }
            }
        }
    }

    if (!empty(trim($_POST["name"]))) {
        $name = trim($_POST["name"]);
    }

    if (!empty(trim($_POST["lastname"]))) {
        $lastname = trim($_POST["lastname"]);
    }

    // Check if username is empty
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter username.";
    } else {
        $username = trim($_POST["username"]);
    }

    // Validate password
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter a password.";
    } elseif (strlen(trim($_POST["password"])) < 6) {
        $password_err = "Password must have atleast 6 characters.";
    } else {
        $password = trim($_POST["password"]);
    }

    // Validate confirm password
    if (empty(trim($_POST["confirm_password"]))) {
        $confirm_password_err = "Please confirm password.";
    } else {
        $confirm_password = trim($_POST["confirm_password"]);
        if (empty($password_err) && ($password != $confirm_password)) {
            $confirm_password_err = "Password did not match.";
        }
    }

    // Check if email is valid
    if (empty(trim($_POST["email"]))) {
        $email_err = "Please enter your email.";
    } elseif (!filter_var(($_POST["email"]), FILTER_VALIDATE_EMAIL)) {
        $email_err = "Please enter a valid email address to contact you.";
    } else {
        $email = trim($_POST["email"]);
    }

    // Check input errors before inserting in database
    if (empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($email_err)) {

        // Set parameters
        $param['name'] = $name;
        $param['lastname'] = $lastname;
        $param['username'] = $username;
        $param['password'] = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
        $param['email'] = $email;

        $result = UserController::add($param);

        if ($result > 0) {
            // Redirect to login page
            header("location: login.php");
        } else {
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="../css/style.css">
    <style>
        body {
            font: 14px sans-serif;
        }

        .wrapper {
            width: 360px;
            padding: 20px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <h2><?php echo $title ?></h2>
        <p>Please fill this form.</p>
        <form id="register_form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">

            </div>
            <div class="form-group">
                <label>lastname</label>
                <input type="text" name="lastname" class="form-control <?php echo (!empty($lastname_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $lastname; ?>">

            </div>
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" required class="required form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" required class="required form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" required class="required form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $confirm_password; ?>">
                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <label>email</label>
                <input type="email" name="email" required class="required form-control <?php echo (!empty($email_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $email; ?>">
                <span class=" invalid-feedback"><?php echo $email_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
            <?php echo (!isset($_SESSION["EDIT"])) ? '<p>Already have an account? <a href="login.php">Login here</a>.</p>' : ''; ?>
        </form>
    </div>
</body>

</html>
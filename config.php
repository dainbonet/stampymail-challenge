<?php

define('DB_SERVER', '172.20.0.4');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'qwerty');
define('DB_NAME', 'stampymail');
 
/* Attempt to connect to MySQL database */
$mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
 
// Check connection
if($mysqli === false){
    die("ERROR: Could not connect. " . $mysqli->connect_error);
}

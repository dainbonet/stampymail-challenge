<?php
// namespace App;
// use App\User as User;
// use App\DataBase as DataBase;
require_once '../functions.php';
class UserController
{

	public static function add($param){
		$user = new User($param);
        $db = new DataBase();
        $result = array();
        $table = $user->get_table();
        $data_types = $user->get_data_types();
        $columns = $user->get_fields();
        unset($columns[0]);
        if(!(empty($param["username"]) && empty($param["pass"]) 
        && empty($param["email"]))){
            $result["id"] = $db->insert($table,$columns,$param,$data_types);
            $result["error"] = $db->get_error();
        }
		return $result;
	}

	public static function update($param){
        $user = new User($param);
        $db = new DataBase();
        $result = array();
        $table = $user->get_table();
        $data_types = $user->get_data_types();
        $columns = $user->get_fields();
        $id = $user->get_idUser();

        if (!(empty($param["id_user "]) && empty($param["username"]) && empty($param["pass"])
            && empty($param["email"]))) {
            $where['id_user'] = $id;
            $data_types .= 'i';
            $result["id"] = $db->update($table, $columns, $param, $data_types,$where);
            $result["error"] = $db->get_error();
        }	
        return $result;
    }

	public static function delete($id){
        $param =array();
        $user = new User($param);
		$db = new DataBase();
        $result = array();

        $table = $user->get_table();
        if(!empty($id)){
            $data_types = 'i';
            $where["id_user"] = $id;
            $result["ret"] = $db->delete($table,$where,$data_types);
            $result["error"] = $db->get_error();
        }
        return $result;
	}

	public static function get_by_id($id){
        $param = array();
		$user = new User($param);
		$db = new DataBase();
        $user_result = array();

        $table = $user->get_table();
        $columns = $user->get_fields();

        if (!empty($id)) {
            $data_types = 'i';
            $where["id_user"] = $id;
            $result = $db->fetch($table, $columns, $where, $data_types);
            if ($result) {
                $user_result["user"] = new User($result[0]);
            }
            $user_result["error"] = $db->get_error();
        }
		return $user_result;
	}

    public static function get_by_username($username)
    {
        $param = array();
        $user = new User($param);
        $db = new DataBase();
        $user_result = array();

        $table = $user->get_table();
        $columns = $user->get_fields();

        if (!empty($username)) {
            $where["username"] = $username;
            $data_types = 's';
            $result = $db->fetch($table, $columns, $where, $data_types);
            if($result){
                $user_result["user"] = new User($result[0]);
            }
            $user_result["error"] = $db->get_error();
        }
        return $user_result;
    }

	public static function getAll()
	{
        $param = array();
        $user = new User($param);
        $db = new DataBase();

        $table = $user->get_table();
        $columns = '*';
        $data_types = "";
        $where = [];
        $user_result = array();
        $result = $db->fetch($table, $columns, $where, $data_types);
        if(!empty($result)){
            foreach ($result as $key ) {
                $user_result["col"][] = new User($key);
            }
        }
        $user_result["error"] = $db->get_error();
        return $user_result;
	}
}




	
?>
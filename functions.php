<?php

function load_model($class_name)
{
    $path_to_file = $_SERVER['DOCUMENT_ROOT'] . '/model/' . $class_name . '.php';

    if (file_exists($path_to_file)) {
        require $path_to_file;
    }
}

function load_controller($service_name)
{
    $path_to_file = $_SERVER['DOCUMENT_ROOT'] . '/controller/' . $service_name . '.php';

    if (file_exists($path_to_file)) {
        require $path_to_file;
    }
}

function load_view($service_name)
{
    $path_to_file = $_SERVER['DOCUMENT_ROOT'] . '/view/' . $service_name . '.php';

    if (file_exists($path_to_file)) {
        require $path_to_file;
    }
}

spl_autoload_register('load_model');
spl_autoload_register('load_controller');
spl_autoload_register('load_view');

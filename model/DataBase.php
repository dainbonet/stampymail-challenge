<?php

// namespace App;
require_once '../functions.php';
// use mysqli;

class DataBase
{

    private $db;
    private $error;

    public function __construct()
    {
        require_once '../config.php';
        /* Attempt to connect to MySQL database */
        $this->db = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

        // Check connection
        if ($this->db === false) {
            $this->error = "ERROR: Could not connect. " . $mysqli->connect_error;
        }
    }

    public function insert($table, $columns, $data, $data_types)
    {
        $cols = "(" . implode(" ,", array_values($columns)) . ")";
        $ret = -1;
        $placeholders = str_repeat('?,', count($columns) - 1) . '?';
        $sql = "INSERT INTO $table $cols VALUES ($placeholders)";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param($data_types,...array_values($data));
            if ($stmt->execute()) {
                $ret = $this->db->insert_id;
                $this->error = "";
            } else {
                $this->error = $this->db->error;
            }
            // Close statement
            $stmt->close();
        }
        return $ret;
    }

    public function update($table, $columns, $data, $data_types, $where)
    {
        $ret = -1;
        $set = "";
        $w = "";
        $values = $data;
        // Prepare '?' for SET
        foreach ($columns as $col) {
            $set .= $col . " = ?, ";
        }
        $set = mb_substr($set, 0, -2);
        // Prepare '?' for WHERE
        foreach ($where as $col => $value) {
            $w .= $col . " = ? AND ";
            $values[] = $value;
        }
        $w = mb_substr($w, 0, -4);
        $sql = "UPDATE $table SET $set WHERE $w";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param($data_types, ...$values);
            if ($stmt->execute()) {
                $ret = $this->db->insert_id;
                $this->error = "";
            } else {
                $this->error = $this->db->error;
            }
            // Close statement
            $stmt->close();
        }
        return $ret;
    }

    public function delete($table, $where, $data_types)
    {
        $ret = false;
        $w = "";
        $values = array();

        // Prepare '?' for WHERE
        foreach ($where as $col => $value) {
            $w .= $col . " = ? AND ";
            $values[] = $value;
        }
        $w = mb_substr($w, 0, -4);
        $sql = "DELETE FROM $table WHERE $w";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param($data_types, ...$values);
            if ($stmt->execute()) {
                $ret = true;
                $this->error = "";
            } else {
                $this->error = $this->db->error;
            }
            // Close statement
            $stmt->close();
        }
        return $ret;
    }

    public function fetch($table, $columns, $where, $data_types)
    {
        $ret = -1;
        $cols = "";
        $w = "";
        $values = array();
        $row = array();
        $result = array();
        if ($columns != '*') {
            // Prepare '?' for COLUMNS NAME
            foreach ($columns as $col) {
                $cols .= $col . ", ";
            }
            $cols = mb_substr($cols, 0, -2);
        } else {
            $cols = "*";
        }

        // Prepare '?' for WHERE
        if (!empty($where)) {
            $w = "WHERE ";
            foreach ($where as $col => $value) {
                $w .= $col . " = ? AND ";
                $values[] = $value;
            }
            $w = mb_substr($w, 0, -4);
        }
        $sql = "SELECT $cols FROM $table $w";
        if ($stmt = $this->db->prepare($sql)) {
            if(!empty($values)){
                $stmt->bind_param($data_types, ...$values);
            }
            if ($stmt->execute()) {
                $meta = $stmt->result_metadata();
                while ($field = $meta->fetch_field()) {
                    $params[] = &$row[$field->name];
                }

                call_user_func_array(array($stmt, 'bind_result'), $params);
                while ($stmt->fetch()) {
                    foreach ($row as $key => $val) {
                        $temp[$key] = $val;
                    }
                    $result[] = $temp;
                    $this->error = "";
                }
            } else {
                $this->error = $this->db->error;
            }
            // Close statement
            $stmt->close();
        }
        return $result;
    }

    public function query($query)
    {
        return $this->db->query($query);
    }

    public function get_error()
    {
        return $this->error;
    }

    public function __destruct()
    {
        $this->db->close();
        $this->db = null;
        $this->error = null;
    }
}

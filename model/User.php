<?php
// namespace App;

class User{

	private $table;
	private $fields;
	private $username;
	private $name;
	private $last_name;
	private $email;
	private $pass;
	private $iduser;
	private $data_types;

	public function __construct($param){
		$this->table = "user";
		$this->fields = ["id_user","name","last_name","username","pass","email"];
		$this->data_types = "sssss";
		$this->username = (isset($param['username'])) ? $param['username'] : null;
		$this->iduser = (isset($param['id_user'])) ? $param['id_user'] : null;
		$this->name = (isset($param['name']))?$param['name'] : null;
		$this->last_name = (isset($param['last_name'])) ? $param['last_name'] : null;
		$this->pass = (isset($param['pass'])) ? $param['pass'] : null;
		$this->email = (isset($param['email'])) ? $param['email'] : null;
	}

	public function get_table(){
		return $this->table;
	}
	
	public function get_fields(){
		return $this->fields;
	}

	public function set_idUser($param){
		$this->iduser = $param;
	}

	public function get_idUser(){
		return $this->iduser;
	}

	public function set_name($param){
		$this->name = $param;
	}

	public function get_name(){
		return $this->name;
	}

	public function set_lastName($param)
	{
		$this->last_name = $param;
	}

	public function get_lastName()
	{
		return $this->last_name;
	}
	public function setUsername($param){
		$this->username = $param;
	}

	public function get_username(){
		return $this->username;
	}

	public function set_email($param){
		$this->email = $param;
	}

	public function get_email(){
		return $this->email;
	}

	public function set_pass($param){
		$this->pass = $param;
	}

	public function validate_password($pass)
	{
		$hashed_pass = $this->pass;
		$res = password_verify($pass, $hashed_pass);
		return $res;
	}

	public function get_data_types(){
		return $this->data_types;
	}

	public function set_data_types($data_types){
		$this->data_types = $data_types;
	}
}
?>

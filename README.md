# Stampymail Challenge

    Web sencilla que gestiona los datos de un usuario. Para ello se realizó un crud en php.



Se requiere 🐘 php 7.4+ instalado.

## Instrucciones

Clonar el repositorio

```bash
git clone /gitlab.com/dainbonet/stampymail-challenge.git
```


Para generar la base de datos esta disponible una [exportación de sql](https://gitlab.com/dainbonet/stampymail-challenge/-/snippets/2161377)


*Optativo: utilizar un contenedor*

Si ya esta instalado [docker](https://docs.docker.com/get-docker/) 
Está disponible un [docker-compose](https://gitlab.com/dainbonet/stampymail-challenge/-/snippets/2161376) para ejecutar el proyecto, también se puede obtener mediante bash:
```bash
wget https://gitlab.com/dainbonet/stampymail-challenge/-/snippets/2161376 
```
Ahora sólo resta ejecutar

```bash
docker-compose -f "docker-compose.yml" up -d --build
```


## 💻 Tecnologias

    PHP
    Js
    CSS
    MySQL/MariaDB
